from django.db import models

class Category(models.Model):

    name = models.CharField(max_length=120)
    slug = models.SlugField()

    def __str__(self):
        return self.name


class Post(models.Model):

    category = models.ForeignKey(Category, related_name='category', on_delete=models.CASCADE)
    title = models.CharField(max_length=120)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    
