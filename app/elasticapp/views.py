from django.shortcuts import render
from django.views import View
from .documents import PostDocument 


class ElasticSearchView(View):

    template_name = 'search.html'

    def get(self, request, *args, **kwargs):
        query = request.GET.get('q')

        try:
            search_result = PostDocument.search().query("match", content=query)
        except:
            search_result = ''

        return render(request, self.template_name, {'search_result': search_result})


