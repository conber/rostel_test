from django_elasticsearch_dsl import DocType, Index
from .models import Post


post = Index('posts')

@post.doc_type
class PostDocument(DocType):

    class Django:
        model = Post
        fields = [
            'title',
            'content',
            'timestamp'
        ]