from django.urls import path
from django.views.generic import TemplateView 
from .views import ElasticSearchView 


urlpatterns = [
    path('', TemplateView.as_view(template_name='base.html')),
    path('elasticsearch_results/', ElasticSearchView.as_view(), name='elasticsearch_results')
]